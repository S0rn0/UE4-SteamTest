// Copyright 2018 Derek Fletcher, All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class SteamTestTarget : TargetRules
{
	public SteamTestTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		ExtraModuleNames.Add("SteamTest");
	}
}
