// Copyright 2018 Derek Fletcher, All Rights Reserved.

#include "SteamTestGameInstance.h"
#include "OnlineSessionInterface.h"
#include "OnlineSessionSettings.h"
#include "GameFramework/PlayerController.h"

const static FName SESSION_NAME = TEXT("SteamTest");

void USteamTestGameInstance::Init()
{
	IOnlineSubsystem* Subsystem = IOnlineSubsystem::Get();
	if (ensure(Subsystem != nullptr))
	{
		UE_LOG(LogTemp, Warning, TEXT("Found subsystem %s"), *Subsystem->GetSubsystemName().ToString());

		SessionInterface = Subsystem->GetSessionInterface();

		SessionSearch = MakeShareable(new FOnlineSessionSearch());
		SessionSearch->bIsLanQuery = true;

		if (ensure(SessionInterface.IsValid()))
		{
			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &USteamTestGameInstance::OnCreateSessionComplete);
			SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &USteamTestGameInstance::OnDestroySessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &USteamTestGameInstance::OnFindSessionsComplete);
		}
	}
	else 
	{
		UE_LOG(LogTemp, Warning, TEXT("No subsystem found"));
	}
}

void USteamTestGameInstance::Host() 
{
	UE_LOG(LogTemp, Warning, TEXT("Hosting"));

	if (ensure(SessionInterface.IsValid()))
	{
		FNamedOnlineSession* CurrentSession = SessionInterface->GetNamedSession(SESSION_NAME);

		// Create the session if none exists, otherwise destroy the current session first
		if (!CurrentSession) 
		{
			CreateSession();
		} 
		else 
		{
			SessionInterface->DestroySession(SESSION_NAME);
		}
	}
}

void USteamTestGameInstance::Join() 
{
	UE_LOG(LogTemp, Warning, TEXT("Joining"));
}

void USteamTestGameInstance::FindSessions()
{
	UE_LOG(LogTemp, Warning, TEXT("Searching for sessions"));
	if (ensure(SessionSearch.IsValid()))
	{
		SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
	}
}

void USteamTestGameInstance::CreateSession() 
{
	UE_LOG(LogTemp, Warning, TEXT("Creating session"));
	if (ensure(SessionInterface.IsValid()))
	{
		FOnlineSessionSettings SessionSettings;
		SessionSettings.bIsLANMatch = true;
		SessionSettings.NumPublicConnections = 2;
		SessionSettings.bShouldAdvertise = true;

		SessionInterface->CreateSession(0, SESSION_NAME, SessionSettings);
	}
}

void USteamTestGameInstance::OnCreateSessionComplete(FName SessionName, bool Success)
{
	if (Success) 
	{
		UE_LOG(LogTemp, Warning, TEXT("Complete creating session"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed creating session"));
	}
}

void USteamTestGameInstance::OnDestroySessionComplete(FName SessionName, bool Success)
{
	if (Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Complete destroying session"));
		if (Success) CreateSession();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed destroying session"));
	}
}

void USteamTestGameInstance::OnFindSessionsComplete(bool Success)
{
	if (Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Complete finding sessions"));
		APlayerController* PC = GetFirstLocalPlayerController();

		// Log the results to the console
		if (ensure(PC) && ensure(SessionSearch.IsValid())) 
		{
			for (const FOnlineSessionSearchResult& SearchResult : SessionSearch->SearchResults) {
				PC->ClientMessage("Session ID: " + SearchResult.GetSessionIdStr());
			}
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Failed finding sessions"));
	}
}
