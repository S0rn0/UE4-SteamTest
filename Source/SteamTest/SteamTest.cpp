// Copyright 2018 Derek Fletcher, All Rights Reserved.

#include "SteamTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SteamTest, "SteamTest" );
 