// Copyright 2018 Derek Fletcher, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SteamTestGameMode.generated.h"

UCLASS(minimalapi)
class ASteamTestGameMode : public AGameModeBase
{
	GENERATED_BODY()
};



