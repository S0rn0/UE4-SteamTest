// Copyright 2018 Derek Fletcher, All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineSubsystem.h"

#include "SteamTestGameInstance.generated.h"

UCLASS()
class STEAMTEST_API USteamTestGameInstance : public UGameInstance
{
	GENERATED_BODY()


	// UGameInstance
public:

	virtual void Init() override;
	// End UGameInstance

public:

	/*
		Console command - Used to jost new sessions. Will destroy existing sessions.
	*/
	UFUNCTION(Exec)
	void Host();

	/*
		Console command - Used to join sessions.
	*/
	UFUNCTION(Exec)
	void Join();

	/*
		Console command - Will find game sessions
	*/
	UFUNCTION(Exec)
	void FindSessions();

private:

	// Stored pointer to the session interface
	IOnlineSessionPtr SessionInterface;
	// The search params and any results from the last search
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;

	/*
		Creates a new online session
	*/
	void CreateSession();

	/*
		Bound to the OnCreateSessionCompleteDelegates
		Handles what occurs once the session has been created
	*/
	void OnCreateSessionComplete(FName SessionName, bool Success);

	/*
		Bound to the OnDestroySessionCompleteDelegates
		This function will create a session once a session has been destroyed.
	*/
	void OnDestroySessionComplete(FName SessionName, bool Success);

	/*
		Bound to the OnFindSessionsCompleteDelegates
		Handles the completion of the session search
	*/
	void OnFindSessionsComplete(bool Success);
};
