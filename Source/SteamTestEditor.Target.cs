// Copyright 2018 Derek Fletcher, All Rights Reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class SteamTestEditorTarget : TargetRules
{
	public SteamTestEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		ExtraModuleNames.Add("SteamTest");
	}
}
